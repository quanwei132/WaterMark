package word2pdf;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

public class HtmlTopdfUtils {

	 /**
     * html文件转pdf文件
     * @param htmlUrl    html路径
     * @param pdfUrl     pdf路径
     * @throws Exception
     */
    public static void html2PDF(String htmlUrl,String pdfUrl) throws Exception {
    
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfUrl));
        document.open();
        XMLWorkerHelper.getInstance().parseXHtml(writer, document, new FileInputStream(htmlUrl));
        document.close(); 
	}
    
    public static void main(String[] args) {
		try {
			html2PDF("D:/trswcm.html", "E:/123.pdf");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
