package word2pdf;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {
	
	public PropertiesUtil() {
	}
	public static String  getProperties(String name, String path) {
		try {
			Properties properties = new Properties();
			InputStream iStream = PropertiesUtil.class.getClassLoader().getResourceAsStream(path);
			properties.load(iStream);
			return properties.getProperty(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static String getWord2pdfProperties(String name) {
		try {
			Properties properties = new Properties();
			InputStream iStream = PropertiesUtil.class.getClassLoader().getResourceAsStream("word2pdf/word2pdf.properties");
			//System.out.println( PropertiesUtil.class.getClassLoader().getResource("/word2pdf/word2pdf.properties"));
			properties.load(iStream);
			System.out.println("properties " + name + " = " + properties.getProperty(name));
			return properties.getProperty(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
