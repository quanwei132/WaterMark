package word2pdf;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Word2pdfStater {

	public Word2pdfStater() {
		// TODO Auto-generated constructor stub
	}
	private static String sourceFloderPath = "pdf.sourceFolder";
	private static String destFloderPath = "html.outputFolder";
	public static String  DOC ="doc"; 
	public static String  HTML ="html";
	public static String  PDF ="pdf";
	
	public static void main(String[] args) throws Exception {
		//System.out.println(PropertiesUtil.class.getResource("").getPath());
		String sourceFolder = PropertiesUtil.getWord2pdfProperties(Word2pdfStater.sourceFloderPath);
		String destFloder = PropertiesUtil.getWord2pdfProperties(Word2pdfStater.destFloderPath);
		Word2pdfStater word2pdfStater = new Word2pdfStater();
		Html2Pdf html2Pdf =new Html2Pdf();
		List<File> docFiles = word2pdfStater.getDocFile(sourceFolder);
		for (File pdfFile : docFiles) {
			String sDestFile = destFloder + pdfFile.getName().replaceAll(DOC, HTML); 
			Word2Html.convert2Html(pdfFile.getAbsolutePath(), sDestFile);
			html2Pdf.convertHtmlToPdf(sDestFile, sDestFile.replace(HTML, PDF));
			System.out .println(sDestFile);
		}
	}
	
	public List<File> getDocFile(String fileFolderPath) throws FileNotFoundException {
		File pdfFolder = new File(fileFolderPath);
		List<File> pdfFileList = new ArrayList<File>();
		if(pdfFolder.exists()) {
			for (File pdfFile : pdfFolder.listFiles()) {
				if(pdfFile.getName().endsWith(".doc")) {
					pdfFileList.add(pdfFile);
				}
			}
		}else {
			throw new FileNotFoundException();
		}
		return pdfFileList;
	}
}
