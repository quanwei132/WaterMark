package com.trs.wsg.pdf.itext;

import java.io.FileOutputStream;

import org.apache.xmlbeans.impl.values.StringEnumValue;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

public class PdfAddWaterMark {

	public PdfAddWaterMark() {
		// TODO Auto-generated constructor stub
	}

	/**
     * 
     * 【功能描述：添加图片和文字水印】 【功能详细描述：功能详细描述】
     * @param srcFile 待加水印文件
     * @param destFile 加水印后存放地址
     * @param text 加水印的文本内容
     * @param textWidth 文字横坐标
     * @param textHeight 文字纵坐标
     * @throws Exception
     */
    private void addWaterMark(String srcFile, String destFile, String text,
            int textWidth, int textHeight) throws Exception
    {
        // 待加水印的文件
        PdfReader reader = new PdfReader(srcFile);
        // 加完水印的文件
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(
                destFile));
        int total = reader.getNumberOfPages() + 1;
        PdfContentByte content;
        // 设置字体
       // BaseFont font = BaseFont.createFont();
        BaseFont basefont = BaseFont.createFont("c:/windows/fonts/SIMYOU.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        //设置透明度
        PdfGState gs = new PdfGState(); 
        gs.setFillOpacity(1f); 
        gs.setStrokeOpacity(1f);
        // 循环对每页插入水印
        for (int i = 1; i < total; i++)
        {
            // 水印的起始
            content = stamper.getUnderContent(i);
            // 开始
            content.beginText();
            // 设置颜色 默认为蓝色
            content.setColorFill(BaseColor.GRAY);
            // content.setColorFill(Color.GRAY);
            // 设置字体及字号
            content.setFontAndSize(basefont, 38);
            // 设置起始位置
            // content.setTextMatrix(400, 880);
            content.setTextMatrix(textWidth, textHeight);
            // 开始写入水印
            content.showTextAligned(Element.ALIGN_LEFT, text, textWidth,
                    textHeight, 45);
            content.endText();
            
            //t添加图片水印
            Image img2 = Image.getInstance("D:\\s.png");  
            img2.setRotationDegrees(45);
           /* image.setAbsolutePosition(50, 400);//坐标  
            image.setRotation(-20);//旋转 弧度  
            image.setRotationDegrees(-45);//旋转 角度  
           	image.scaleAbsolute(200,100);//自定义大小  
            image.scalePercent(50);//依照比例缩放  
*/            img2.setAbsolutePosition(0, 0);  
            content.addImage(img2);
        }
        stamper.close();
    }
    
    public void addWaterMark(String srcFile, String destFile, String waterMarkFile) throws Exception
    {
        // 待加水印的文件
        PdfReader reader = new PdfReader(srcFile);
        // 加完水印的文件
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(
                destFile));
        int total = reader.getNumberOfPages() + 1;
        PdfContentByte content;
        // 设置字体
        // BaseFont font = BaseFont.createFont();
        //BaseFont basefont = BaseFont.createFont("./resource/simyou.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        //设置透明度
        PdfGState gs = new PdfGState(); 
        gs.setFillOpacity(1f); 
        gs.setStrokeOpacity(1f);
        // 循环对每页插入水印
        for (int i = 1; i < total; i++)
        {
        	if(i != 2  && i != total-1 ) continue;
            // 水印的起始
            content = stamper.getUnderContent(i);
            // 开始
           /* content.beginText();
            // 设置颜色 默认为蓝色
            content.setColorFill(BaseColor.GRAY);
            // content.setColorFill(Color.GRAY);
            // 设置字体及字号
            content.setFontAndSize(basefont, 38);
            // 设置起始位置
            // content.setTextMatrix(400, 880);
            content.setTextMatrix(textWidth, textHeight);
            // 开始写入水印
            content.showTextAligned(Element.ALIGN_LEFT, text, textWidth,
                    textHeight, 45);
            content.endText();*/
            
            //t添加图片水印
            Image img2 = Image.getInstance(waterMarkFile);  
            img2.setRotationDegrees(45);
           /* image.setAbsolutePosition(50, 400);//坐标  
            image.setRotation(-20);//旋转 弧度  
            image.setRotationDegrees(-45);//旋转 角度  
           	image.scaleAbsolute(200,100);//自定义大小  
            image.scalePercent(50);//依照比例缩放  
*/          img2.setAbsolutePosition(0, 0);  
            content.addImage(img2);
        }
        stamper.close();
    }
}
