package com.trs.wsg.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class FileUtil {
	static Logger logger = Logger.getLogger(FileUtil.class);
	public FileUtil() {
		// TODO Auto-generated constructor stub
	}
	public static List<File> getFileListBySuffix(String fileFolderPath, String fileSuffix) throws FileNotFoundException {
		File pdfFolder = new File(fileFolderPath);
		List<File> pdfFileList = new ArrayList<File>();
		if(pdfFolder.exists()) {
			for (File pdfFile : pdfFolder.listFiles()) {
				if(pdfFile.getName().endsWith("." + fileSuffix)) {
					pdfFileList.add(pdfFile);
				}
			}
		}else {
			throw new FileNotFoundException();
		}
		return pdfFileList;
	}
	/**
	 * 
	 * @param fileFolderPath
	 * @param fileSuffixs  without "."
	 * @return
	 * @throws FileNotFoundException
	 */
	public static List<File> getFileListBySuffix(String fileFolderPath, String[] fileSuffixs) throws FileNotFoundException {
		File pdfFolder = new File(fileFolderPath);
		List<File> pdfFileList = new ArrayList<File>();
		if(pdfFolder.exists()) {
			for (File pdfFile : pdfFolder.listFiles()) {
				for (String fileSuffix : fileSuffixs) {
					if(pdfFile.getName().toLowerCase().endsWith("." + fileSuffix)) {
						pdfFileList.add(pdfFile);
						break;
					}
				}
			}
		}else {
			logger.info(pdfFolder.getAbsolutePath());
			throw new FileNotFoundException();
		}
		return pdfFileList;
	}
	
	public static String getSuffix(String path) {
		int begin = path.lastIndexOf(".");
		int end = path.length();
		//System.out.println(String .valueOf(begin) + end +  path.substring(begin+1, end));
		return path.substring(begin + 1, end);
	}
}
