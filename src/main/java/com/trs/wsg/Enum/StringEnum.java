package com.trs.wsg.Enum;

public enum StringEnum {
	 BACK_SLANT("/"),
	 SLANT(System.getProperty("file.separator")),
	 FILE_SEPARATOR(System.getProperty("file.separator")),
	 COMMA(",") ,
	 POINT(".") ,
	 NOT_EXISTS("is not exists !"),
	 ;
	private String param;
	private StringEnum(String param) {
		this.param = param;
	}
	public String getParam() {
		return this.param;
	}
	public String toString() {
		return this.param;
	}
}