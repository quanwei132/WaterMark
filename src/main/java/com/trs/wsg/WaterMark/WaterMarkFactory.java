package com.trs.wsg.WaterMark;


import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Transparency;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import com.trs.wsg.Enum.StringEnum;
import com.trs.wsg.util.FileUtil;

/**
 * @author 白芷
 * @Date 2017/03/12
 * @use 利用Java代码给图片加水印
 */
public class WaterMarkFactory {
	Logger logger = Logger.getLogger(WaterMarkFactory.class);
    /**
     * @param srcImgPath 源图片路径
     * @param tarImgPath 保存的图片路径
     * @param waterMarkContent 水印内容
     * @param markContentColor 水印颜色
     * @param font 水印字体
     */
/*    public void addWaterMark(String srcImgPath, String tarImgPath, String waterMarkContent,Color markContentColor,Font font) {

        try {
            // 读取原图片信息
            File srcImgFile = new File(srcImgPath);//得到文件
            Image srcImg = ImageIO.read(srcImgFile);//文件转化为图片
            int srcImgWidth = srcImg.getWidth(null);//获取图片的宽
            int srcImgHeight = srcImg.getHeight(null);//获取图片的高
            // 加水印
            BufferedImage bufImg = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufImg.createGraphics();
            g.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);
            g.setColor(markContentColor); //根据图片的背景设置水印颜色
            g.setFont(font);              //设置字体

            //设置水印的坐标
            int x = srcImgWidth - 2*getWatermarkLength(waterMarkContent, g);  
            int y = srcImgHeight - 2*getWatermarkLength(waterMarkContent, g);  
            g.drawString(waterMarkContent, x, y);  //画出水印
            g.dispose();  
            // 输出图片  
            FileOutputStream outImgStream = new FileOutputStream(tarImgPath);  
            ImageIO.write(bufImg, "jpg", outImgStream);
            System.out.println("添加水印完成");  
            outImgStream.flush();  
            outImgStream.close();  

        } catch (Exception e) {
        	e.printStackTrace();
        }
    }*/
    
    public void addWaterMark(String srcImgPath, String tarImgPath
    		, String sWaterMarkFile) {
        try {
            // 读取原图片信息
            File srcImgFile = new File(srcImgPath);//得到文件
            Image srcImg = ImageIO.read(srcImgFile);//文件转化为图片
            
            
            File waterMarkFile = new File(sWaterMarkFile);//得到文件
            Image waterMarkImg = ImageIO.read(waterMarkFile);//文件转化为图片
            //com.itextpdf.text.Image waterMarkImg = com.itextpdf.text.Image.getInstance(waterMarkFile.getAbsolutePath());//文件转化为图片
            int srcImgWidth = srcImg.getWidth(null);//获取图片的宽
            int srcImgHeight = srcImg.getHeight(null);//获取图片的高
            // 加水印
            BufferedImage bufImg = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bufImg.createGraphics();
            g.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);
            //设置水印的坐标
            int x = 0;  
            int y = 0;  
            //waterMarkImg.setRotationDegrees(-45);
            g.drawImage(waterMarkImg, 0, 0, waterMarkImg.getWidth(null), waterMarkImg.getHeight(null), null);  //画出水印
            g.dispose();  
            // 输出图片  
            FileOutputStream outImgStream = new FileOutputStream(tarImgPath); 
            logger.info(FileUtil.getSuffix(srcImgPath) + " " + tarImgPath);
            ImageIO.write(bufImg,FileUtil.getSuffix(srcImgPath) , new File(tarImgPath));
            logger.info("添加水印完成  ： " + srcImgPath);  
            outImgStream.flush();  
            outImgStream.close();  

        } catch (Exception e) {
        	e.printStackTrace();
        }
    }
    
    public void createWaterMark(String tarImgPath, String waterMarkContent,Color markContentColor,Font font) {
        try {
            // 根据文本长度决定水印大小
            BufferedImage tempImgForSize = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
            Graphics2D g = tempImgForSize.createGraphics();
            g.setFont(font);
            int srcImgHeigth =  3*getWatermarkheigth(waterMarkContent, g);  
            int srcImgWidth = 100 + getWatermarkLength(waterMarkContent, g); 
            g.dispose();
            BufferedImage bufImg = 
            	new BufferedImage(srcImgWidth, srcImgHeigth, BufferedImage.TYPE_INT_RGB);
            g = bufImg.createGraphics();
            bufImg = g.getDeviceConfiguration()
            		.createCompatibleImage(srcImgWidth, srcImgHeigth, Transparency.TRANSLUCENT);  
            g.dispose();  
            g = bufImg.createGraphics();  
            g.setColor(markContentColor); //根据图片的背景设置水印颜色
            g.setFont(font);              //设置字体
            //设置水印的坐标
            int x = 50;
            int y = srcImgHeigth / 2;
            //System.out.println("x="+ x +" y="+ y + " srcImgWidth=" + srcImgWidth);
            g.drawString(waterMarkContent, x, y);  //画出水印
            g.dispose();  
            //缩放
            Image Itemp = bufImg.getScaledInstance(srcImgWidth/2, srcImgHeigth/2, bufImg.SCALE_SMOOTH);//设置缩放目标图片模板
            
            double wr = 0.5;   double hr= 0.5;   //获取缩放比例
            //hr=h*1.0 / bufImg.getHeight();

            AffineTransformOp ato = new AffineTransformOp(AffineTransform.getScaleInstance(wr, hr), null);
            Itemp = ato.filter(bufImg, null);
            
            // 输出图片
            String tarImgParentPath = tarImgPath
            	.substring(0, tarImgPath.lastIndexOf(StringEnum.FILE_SEPARATOR.getParam()));
            if(! new File(tarImgParentPath).exists()) {
    			new File(tarImgParentPath).mkdirs();
    		}
            FileOutputStream outImgStream = new FileOutputStream(tarImgPath);  
            logger.info(tarImgPath);
            ImageIO.write((BufferedImage)Itemp, "png", new File(tarImgPath));
            logger.info("生成水印完成 ： " + tarImgPath);  
            outImgStream.flush();  
            outImgStream.close();  
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }
    public int getWatermarkLength(String waterMarkContent, Graphics2D g) {  
        return g.getFontMetrics(g.getFont())
        		.charsWidth(waterMarkContent.toCharArray(), 0, waterMarkContent.length());  
    }  
    public int getWatermarkheigth(String waterMarkContent, Graphics2D g) {  
        return g.getFontMetrics(g.getFont()).charsWidth(waterMarkContent.toCharArray(), 0,1);  
    }
    public static void main(String[] args) {
        Font font = new Font("微软雅黑", Font.PLAIN, 35);                     //水印字体
        String srcImgPath="D:/s.jpg"; //源图片地址
        String tarImgPath="D:/t.png"; //待存储的地址
        String tarImgPath2="D:/t.jpg"; //待存储的地址
        String waterMarkContent="文史馆zyc";//水印内容
        	
        Color color=new Color(200,200,200,80);   
        //水印图片色彩以及透明度
        //new WaterMarkFactory().addWaterMark(srcImgPath, tarImgPath2, waterMarkContent, color,font);
        new WaterMarkFactory().createWaterMark(tarImgPath, waterMarkContent, color,font);
    }
}