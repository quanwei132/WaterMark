package com.trs.wsg.WaterMark.start;

import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.omg.CORBA.WStringSeqHelper;

import com.trs.wsg.Enum.StringEnum;
import com.trs.wsg.WaterMark.WaterMarkFactory;
import com.trs.wsg.pdf.itext.PdfAddWaterMark;
import com.trs.wsg.util.FileUtil;
import com.trs.wsg.video.ffmpeg.FFMPEG;

import word2pdf.PropertiesUtil;

public class WaterMarkStater {
	static Logger logger = Logger.getLogger(WaterMarkStater.class);
	final static String waterMarkPath = "word2pdf/waterMark.properties";
	final static String pdfSourceFolder = "pdf.sourceFolder";
	final static String videoSourceFolder = "video.sourceFolder";
	final static String pictureSourceFolder = "picture.sourceFolder";
	final static String SOURCE_FOLDER = "sourceFolder";
	final static String DEST_FOLDER = "destFolder";
	final static String FFMPEG_PATH = "ffmpeg_path";
	
	public WaterMarkStater() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		//PropertyConfigurator.configure(".\\resource\\log4j.properties");
		//System.out.println(WaterMarkStater.class.getClassLoader().getResource("."));
		InputStream inputStream = WaterMarkStater.class.getResourceAsStream("/word2pdf/log4j.properties");
		PropertyConfigurator.configure(inputStream);
		logger.info("log4j");
		/*String pdfSourceFolder =  PropertiesUtil
				.getProperties(WaterMarkStater.pdfSourceFolder, WaterMarkStater.waterMarkPath);
		String videoSourceFolder =  PropertiesUtil
				.getProperties(WaterMarkStater.videoSourceFolder, WaterMarkStater.waterMarkPath);
		String pictureSourceFolder = PropertiesUtil
				.getProperties(WaterMarkStater.pictureSourceFolder, WaterMarkStater.waterMarkPath);
		*/
		String sourceFolder = PropertiesUtil
				.getProperties(WaterMarkStater.SOURCE_FOLDER, WaterMarkStater.waterMarkPath);
		String destFolder = PropertiesUtil
				.getProperties(WaterMarkStater.DEST_FOLDER, WaterMarkStater.waterMarkPath);
		WaterMarkStater waterMarkStater = new WaterMarkStater();
		//生成水印图片  D:\waterMark\destination\logo、logo.png
		String waterMarkContent = 
				"wsg_" + new Date().getTime() +"_" + UUID.randomUUID().toString().replaceAll("-", "");
		String waterMarkFile = waterMarkStater.createWaterMarkPic(waterMarkContent);
		//为图片打水印
		try {
			logger.info("pictureAddWaterMark");
			String picSourceFolder = sourceFolder + StringEnum.FILE_SEPARATOR.getParam()+ "picture";
			String picDestFolder = destFolder + StringEnum.FILE_SEPARATOR.getParam()+ "picture";
			if( ! new File(picDestFolder).exists()) new File(picDestFolder).mkdirs();
			if( ! new File(picSourceFolder).exists()) new File(picSourceFolder).mkdirs();
			waterMarkStater.pictureAddWaterMark(picSourceFolder, picDestFolder, waterMarkFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			logger.info("pdfAddWaterMark");
			String pdfSourceFolder = sourceFolder + StringEnum.FILE_SEPARATOR.getParam()+ "pdf";
			String pdfDestFolder = destFolder + StringEnum.FILE_SEPARATOR.getParam()+ "pdf";
			if( ! new File(pdfDestFolder).exists()) new File(pdfDestFolder).mkdirs();
			if( ! new File(pdfSourceFolder).exists()) new File(pdfSourceFolder).mkdirs();
			waterMarkStater.pdfAddWaterMark(pdfSourceFolder, pdfDestFolder, waterMarkFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			logger.info("videoAddWaterMark");
			String videoSourceFolder = sourceFolder + StringEnum.FILE_SEPARATOR.getParam()+ "video";
			String videoDestFolder = destFolder + StringEnum.FILE_SEPARATOR.getParam()+ "video";
			if( ! new File(videoDestFolder).exists()) new File(videoDestFolder).mkdirs();
			if( ! new File(videoSourceFolder).exists()) new File(videoSourceFolder).mkdirs();
			waterMarkStater.videoAddWaterMark(videoSourceFolder, videoDestFolder, waterMarkFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void videoAddWaterMark(String sourceFolder, String destFolder, String waterMarkFile) throws Exception {
		String[] fileSuffix = {"mp4","avi","rm"};
		String FFMPEG = PropertiesUtil
				.getProperties(WaterMarkStater.FFMPEG_PATH, WaterMarkStater.waterMarkPath);
		List<File> videotureFileList = FileUtil.getFileListBySuffix(sourceFolder, fileSuffix);
		for (File videoFile : videotureFileList) {
			String srcVideoPath = videoFile.getAbsolutePath();
			String tarVideoPath = destFolder + StringEnum.FILE_SEPARATOR.getParam() + videoFile.getName();
			HashMap<String, String>dto=new HashMap<String, String>();
			dto.put("ffmpeg_path", FFMPEG);//必填
			dto.put("input_path", srcVideoPath);//必填
			dto.put("video_converted_path", tarVideoPath);//必填
			String waterMarkPngPath = waterMarkFile.replace("\\", "/");
			waterMarkPngPath = waterMarkPngPath.replace(":", "\\\\:");
			dto.put("logo", waterMarkPngPath);
			//可选(注意windows下面的logo地址前面要写4个反斜杠,如果用浏览器里面调用servlet并传参只用两个,如 d:\\:/ffmpeg/input/logo.png)
			String secondsString=  new FFMPEG().videoTransfer(dto);
			
			logger.info("转换共用:"+secondsString+"秒 " + waterMarkFile +waterMarkPngPath);
		}
	}
	
	public void pdfAddWaterMark(String sourceFolder, String destFolder, String waterMarkFile) throws Exception {
		String[] fileSuffix = {"pdf"};
		List<File> pdftureFileList = FileUtil.getFileListBySuffix(sourceFolder, fileSuffix);
		for (File pdfFile : pdftureFileList) {
			String srcPdfPath = pdfFile.getAbsolutePath();
			String tarPdfPath = destFolder + StringEnum.FILE_SEPARATOR.getParam() + pdfFile.getName();
			new PdfAddWaterMark().addWaterMark(srcPdfPath, tarPdfPath, waterMarkFile);
		}
	}
	
	public void pictureAddWaterMark(String sourceFolder, String destFolder, String waterMarkFile) throws FileNotFoundException {
		String[] fileSuffix = {"jpg","png"};
		List<File> pictureFileList = FileUtil.getFileListBySuffix(sourceFolder, fileSuffix);
		for (File picFile : pictureFileList) {
			String srcImgPath = picFile.getAbsolutePath();
			String tarImgPath = destFolder + StringEnum.FILE_SEPARATOR.getParam() + picFile.getName();
			new WaterMarkFactory().addWaterMark(srcImgPath, tarImgPath, waterMarkFile);
		}
	}
	/**
	 * 
	 * @param waterMarkContent 水印内容
	 */
	public String createWaterMarkPic(String waterMarkContent) {
		String destFolder = PropertiesUtil
			.getProperties(WaterMarkStater.DEST_FOLDER, WaterMarkStater.waterMarkPath);
		Font font = new Font("微软雅黑", Font.PLAIN, 35);//水印字体
        String tarImgPath = destFolder + StringEnum.FILE_SEPARATOR.getParam() + "logo" + StringEnum.FILE_SEPARATOR.getParam() + "logo.png"; //待存储的地址
        Color color=new Color(200,200,200,80);
		new WaterMarkFactory().createWaterMark(tarImgPath, waterMarkContent, color,font);
		return tarImgPath;
	}
}
